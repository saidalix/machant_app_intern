
# README #

This is the Android Version of the ChapChap Merchant Application.

### What is this repository for? ###

* Android Developer Team of ChapChap
* Version
* Team Work Development Integration With Slack and Trello

### How do I get set up? ###


* Clone the Repo
* Dependency Configuration in Gradle inside Android Studio and Build
* Create android app which can accept O 26
* Test the App on Nougat Devices + , Also Test it on Lower SDK Versions (Froyo)
* Use Genymotion as your Testing Platform , or a Real Device



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* bitbucket: @chapchap
* github   : https://gitlab.com/chapchap/web_app
* twitter  : https://twitter.com/ChapchapAfrica
* website  : http://chapchap.co/
